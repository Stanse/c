#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "labtest.h"

#define CUR_TICK ((float)(clock())/CLOCKS_PER_SEC*1000.f)
#define MS_IN_MCRS 1000
#define SLEEP_MS 100
#define SLEEP_TIME SLEEP_MS * MS_IN_MCRS
#define SECONDS_TO_RUN_MAX 5

int forkTestWithTimeout(const Test* test)
{
  pid_t child;
  int status = 0;
  double timeout_time;

  if (!test || !test->func)
    return 1;

  switch(child = fork())
  {
  case -1:
    printf("error creating fork\n");
    break;
  case 0://child process
    exit(test->func());
  default: //parent process
    timeout_time = CUR_TICK + test->timeoutSec;
    for (;;)
    {
      const int res = waitpid(child, &status, WNOHANG);
      if (res && WIFEXITED(status)) //state got and process exited
        return WEXITSTATUS(status);
      if (CUR_TICK >= timeout_time) //timeout reached
      {
        kill(child, SIGKILL);
        printf("Interrupted due too long run time: %f/%f\n", CUR_TICK, (double)test->timeoutSec);
        return 1;
      }
      usleep(SLEEP_TIME);
    }
  }
  return 1;
}

int runTests(Test *testList, unsigned int count)
{
  if (!testList)
    return 255;
  for (int i = 0; i < count; ++i)
  {
    if (forkTestWithTimeout(&testList[i]) != 0)
      return i + 1;
  }
  return 0;
}
