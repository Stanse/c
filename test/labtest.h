#ifndef _LAB_TEST_H
#define _LAB_TEST_H

typedef int (*testFunc)();
typedef struct _Test
{
  testFunc func;
  int timeoutSec;
  const char* message;
}Test;

int runTests(Test *testList, unsigned int count);

#endif
