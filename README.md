# general notes

Some content in this repo stored under git-lfs (pdf files, binaries). You need to make sure you have git-lfs installed along with git in order to be able to fetch them. Course materials are constantly updated so, please subscribe to gitlab notifications to see what are the updates in order to be up-to-date.

# practice tasks

Practice subdirectory contains practical tasks materials (keep being updated). There will be 6 small assignments (one for each week) and one of medium size (Huffman archiver) - this is kind of final assignment.
The order in which tasks are given:
1. graph
2. xor
3. littlebig
4. life
5. alligned_allocations
6. ?

Build Platform   | Status (tests only)
---------------- | ----------------------
Fedora 31        | [![pipeline status](https://gitlab.com/Stanse/c/badges/master/pipeline.svg)](https://gitlab.com/Stanse/c/-/commits/master)          


# course materials

Slides subdirectory contains pdf slides of what was shown during the lessons. If you spot any mistakes in slides or not sure about correctness of anything there - please report to me, will be much appreciated. Start reading K&R 2nd edition ASAP if you still haven't done it yet. Look for suggestions in literature.pdf if you want more books to look at.

# development environment

For those who are on windows probably the easiest way to deploy Linux environment is using vagrant with Vagrantfile config provided here. The steps are below:
- install vagrant and virtualbox on your windows machine
- put vagrant file from this repo in some folder, open cmd.exe in this folder and type `vagrant up`, it will download Ubuntu 18.04 image for virtualbox, provision a virtual machine, upgrade it and install some required packages for you
- `vagrant ssh` - will open a remote shell for you to your virtual Linux
- alternatively you can login in to GUI window (login: vagrant, password: vagrant) and issue `startxfce4&` command to have full-fledged UI session
- study `vagrant help` output to get more ideas regarding other useful commands

For those who are on Linux there shouldn't be much troubles, please make sure you have you have all development environment installed (again, you can consult with what Vagrantfile mentions).

There are other options like using Cygwin/MSYS or WSL which might work as well but no any guarantee and you are welcome to try and report whether it works or not.

You can use whatever compiler/debugger/editor you want. If you in doubt or haven't really used anything I strongly suggest using gcc/gdb/vim as this quite universal and suitable for the widest range of platforms. Refer to the documentation online, ask questions in Slack channel if in trouble. As for the gdb I also suggest to print out this stuff: http://users.ece.utexas.edu/~adnan/gdb-refcard.pdf and put it somewhere next your monitor in order to start memorizing most frequently used commands.

# coding style

It is very important to have some settled coding style for all the code you write from the very beginning. You can develop your own style, you can pick up any existing but that's utterly important to have consistency in everything you write. Lack of consistency is always bad sign which means either lack of experience or just bad attitude. In any case it does describes you as a bad developer. If you don't have yet your style or don't want (or cannot yet) create one, stick to some existing. Good style guide can be found here: https://www.kernel.org/doc/html/latest/process/coding-style.html . Read through it, it worth spending time on it. This one is not the only existing, there are more. Google for them.

# help

Use Slack for discussion and asking questions which all of you should have been invited to. Send me a personal email if you completely got stuck and there is no help on Slack channel (or you for some reasons can't access Slack).
