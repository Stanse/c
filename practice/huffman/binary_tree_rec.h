#ifndef BINARY_TREE_REC_H
#define BINARY_TREE_REC_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef uint8_t type_s;
typedef uint64_t type_f;

#define CMP_EQ(a, b) ((a) == (b))
#define CMP_LT(a, b) ((a) < (b))
#define CMP_GT(a, b) ((a) > (b))

typedef struct Node
{
    struct Node* left;
    struct Node* right;
    struct Node *p_next;
    struct Node *p_prev;

    type_f frequency;
    type_s symbol;
    uint8_t isSymbol;
} Node;

Node* get_node(type_s value)
{
    Node* tmp = (Node*) malloc(sizeof(Node));
    tmp->left = tmp->right = NULL;
    tmp->symbol = value;
    tmp->p_next = NULL;
    tmp->p_prev = NULL;
//    if(value == 0)
//        tmp->frequency = 0;
//    else
        tmp->frequency = 1;
     tmp->isSymbol = 1;
    return tmp;
}

Node* copy_node(Node* node)
{
    Node* tmp = (Node*) malloc(sizeof(Node));
    tmp->symbol = node->symbol;
    tmp->frequency = node->frequency;
    tmp->left = node->left;
    tmp->right = node->right;
    tmp->p_next = node->p_next;
    tmp->p_prev = node->p_prev;

    return tmp;
}

Node* insert_to_binary_tree(Node** root, type_s new_symbol)
{
    if (*root == NULL)
    {
        *root = get_node(new_symbol);
        (*root)->isSymbol = 1;
        return *root;
    }
    if (CMP_GT((*root)->symbol, new_symbol))
    {
        (*root)->left = insert_to_binary_tree(&((*root)->left), new_symbol);
        return *root;
    } else if (CMP_LT((*root)->symbol, new_symbol))
    {
        (*root)->right = insert_to_binary_tree(&((*root)->right), new_symbol);
        return *root;
    } else if (CMP_EQ((*root)->symbol, new_symbol))
    {
        (*root)->frequency++;
        return *root;
    } else
    {
        exit(3);
    }
}

Node* get_node_from_bin_tree_by_value(Node* root, type_s value)
{
    if (!root)
        return NULL;

    if (CMP_GT(root->symbol, value))
    {
        get_node_from_bin_tree_by_value(root->left, value);
    }
    else if (CMP_LT(root->symbol, value))
    {
        get_node_from_bin_tree_by_value(root->right, value);
    }
    else
    {
        return root;
    }
}

Node* get_max_node(Node *root)
{
    if (root == NULL)
        exit(4);
    if (root->right)
    {
        return get_max_node(root->right);
    }
    return root;
}

Node* get_min_node(Node* root)
{
    if (root == NULL)
        exit(3);
    if (root->left)
    {
        return get_min_node(root->left);
    }
    return root;
}

Node* remove_node_rec(Node* root, type_s value)
{
    if (root == NULL)
        return NULL;

    if (CMP_GT(root->symbol, value))
    {
        root->left = remove_node_rec(root->left, value);
        return root;
    }
    else if (CMP_LT(root->symbol, value))
    {
        root->right = remove_node_rec(root->right, value);
        return root;
    }
    else
    {
        if (root->left && root->right)
        {
            Node* loc_max = get_max_node(root->left);
            root->symbol = loc_max->symbol;
            root->frequency = loc_max->frequency;
            root->left = remove_node_rec(root->left, loc_max->symbol);
            return root;
        }
        else if (root->left)
        {
            Node *tmp = root->left;
            free(root);
            return tmp;
        }
        else if (root->right)
        {
            Node *tmp = root->right;
            free(root);
            return tmp;
        } else
        {
            free(root);
                       return NULL;
        }
    }
}

void pre_order_travers(Node* root, void (*visit)(Node*, void*), void *params)
{
    if (root)
    {
        visit(root, params);
        pre_order_travers(root->left, visit, params);
        pre_order_travers(root->right, visit, params);
    }
}

void in_order_travers(Node* root, void (*visit)(Node*, void*), void *params)
{
    if (root)
    {
        in_order_travers(root->left, visit, params);
        visit(root, params);
        in_order_travers(root->right, visit, params);
    }
}

void post_order_travers(Node* root, void (*visit)(Node*, void*), void *params)
{
    if (root)
    {
        post_order_travers(root->left, visit, params);
        post_order_travers(root->right, visit, params);
        visit(root, params);
    }
}

void print_node(Node *current)
{
    if(current->isSymbol)
        printf("'%c' = '0x%x' - '%lu' \n" ,current->symbol, current->symbol, current->frequency);
}

void print_tree(Node *root, const char *dir, int level)
{
    if (root)
    {
        if(root->isSymbol)
            printf("lvl %d  %s  =   '%c' = '0x%x' - %lu\n", level, dir, root->symbol, root->symbol, root->frequency);
        else
            printf("lvl %d  %s  =   'node' - %lu\n", level, dir, root->frequency);
        print_tree(root->left, "'left'", level+1);
        print_tree(root->right, "'right'", level+1);
    }
}

void delete_bin_tree(Node **root) {
    if (*root) {
        delete_bin_tree(&((*root)->left));
        delete_bin_tree(&((*root)->right));
        free(*root);
    }
}

//+------------------------------------------------------+
//|                  Double Linked list                  |
//+------------------------------------------------------+
typedef struct List
{
    Node* head;
    Node* tail;
    uint16_t size;
} List;

List* create_list()                                 // CREATE LIST
{
    List *tmp = (List*) malloc(sizeof(List));
    tmp->size = 0;
    tmp->head = tmp->tail = NULL;
    return tmp;
}

void delete_list(List** list)                       // DELETE LIST
{
    Node *tmp = (*list)->head;
    Node *next = NULL;
    while (tmp) {
        next = tmp->p_next;
        free(tmp);
        tmp = next;
    }
    free(*list);
    (*list) = NULL;
}

void push_front(List* list, Node* data)             // PUSH FRONT
{
    Node *tmp = (Node*) malloc(sizeof(Node));
    if (tmp == NULL)
        exit(1);
    tmp->symbol = data->symbol;
    tmp->frequency = data->frequency;
    tmp->isSymbol = data->isSymbol;
    tmp->left = data->left;
    tmp->right = data->right;
    tmp->p_next = list->head;
    tmp->p_prev = NULL;
    if (list->head)
    {
        list->head->p_prev = tmp;
    }
    list->head = tmp;

    if (list->tail == NULL)
    {
        list->tail = tmp;
    }
    list->size++;
}

void pop_front(List *list)                          // POP FRONT
{
    Node *prev;
    if (list->head == NULL)
        exit(2);

    prev = list->head;
    list->head = list->head->p_next;
    if (list->head)
    {
        list->head->p_prev = NULL;
    }
    if (prev == list->tail)
    {
        list->tail = NULL;
    }
    list->size--;
}

void push_back(List* list, Node* data)              // PUSH BACK
{
    Node *tmp = (Node*) malloc(sizeof(Node));
    if (tmp == NULL) {
        exit(3);
    }
    tmp->symbol = data->symbol;
    tmp->frequency = data->frequency;
    tmp->isSymbol = data->isSymbol;
    tmp->left = data->left;
    tmp->right = data->right;
    tmp->p_next = NULL;
    tmp->p_prev = list->tail;
    if (list->tail) {
        list->tail->p_next = tmp;
    }
    list->tail = tmp;

    if (list->head == NULL) {
        list->head = tmp;
    }
    list->size++;
}

void pop_back(List* list)                           // POP BACK
{
    Node *next;
    if (list->tail == NULL)
        exit(4);

    next = list->tail;
    list->tail = list->tail->p_prev;
    if (list->tail)
    {
        list->tail->p_next = NULL;
    }
    if (next == list->head)
    {
        list->head = NULL;
    }
    free(next);

    list->size--;
}

void fill_list(List* list, Node* head)                  // FILL LIST
{
    if(head)
    {
        Node *tmp = (Node*)malloc(sizeof (Node));
        tmp->symbol = head->symbol;
        tmp->frequency = head->frequency;
        tmp->isSymbol = head->isSymbol;
        tmp->left = NULL;
        tmp->right = NULL;
        push_front(list, tmp);
        fill_list(list, head->left);
        fill_list(list, head->right);
    }
}

Node* get_by_index (List* list, uint64_t index)         // GET NODE BY INDEX
{
    Node *tmp = NULL;
    size_t i;

    if (index < list->size/2)
    {
        i = 0;
        tmp = list->head;
        while (tmp && i < index)
        {
            tmp = tmp->p_next;
            i++;
        }
    }
    else
    {
        i = list->size - 1;
        tmp = list->tail;
        while (tmp && i > index)
        {
            tmp = tmp->p_prev;
            i--;
        }
    }
    return tmp;
}

void insert_to_list(List* list, uint64_t index, Node* data)         // INSERT NODE TO LIST BY INDEX
{
    Node *elm = NULL;
    Node *ins = NULL;
    elm = get_by_index(list, index);
    if (elm == NULL)
        exit(5);
    ins = (Node*) malloc(sizeof(Node));
    ins->symbol = data->symbol;
    ins->frequency = data->frequency;
    ins->left = data->left;
    ins->right = data->right;
    ins->p_prev = elm;
    ins->p_next = elm->p_next;
    if (elm->p_next)
    {
        elm->p_next->p_prev = ins;
    }
    elm->p_next = ins;

    if (!elm->p_prev)
    {
        list->head = elm;
    }
    if (!elm->p_next)
    {
        list->tail = elm;
    }

    list->size++;
}

void delete_by_index(List* list, uint64_t index)           // DELETE NODE BY INDEX
{
    Node *elm = NULL;

    elm = get_by_index(list, index);
    if (elm == NULL)
        exit(5);
    if (elm->p_prev)
    {
        elm->p_prev->p_next = elm->p_next;
    }
    if (elm->p_next)
    {
        elm->p_next->p_prev = elm->p_prev;
    }

    if (!elm->p_prev)
    {
        list->head = elm->p_next;
    }
    if (!elm->p_next)
    {
        list->tail = elm->p_prev;
    }

    free(elm);

    list->size--;
}

void print_list(Node* head)                                 // PRINT LIST
{  
    if(head)
    {
        if(head->isSymbol)
        {
            Node *tmp = head;
            printf(" '%c' = '0x%x' - %lu\n", tmp->symbol, tmp->symbol, tmp->frequency);
            print_list(tmp->p_next);
        }
       // if(tmp->p_next == NULL)  printf("\n");
    }
}

void print_code(Node* head)
{
    while(head)
    {
        printf(" %lu", head->frequency);
        head = head->p_next;
    }
     printf("\n");
}

// --------------------- INSERT SORT ---------------------

void insert_before_element(List* list, Node* node_aim_for_insert, Node* data) {
    Node *node_for_insert = NULL;
    if (node_aim_for_insert == NULL) {
        exit(6);
    }

    if (!node_aim_for_insert->p_prev) {
        push_front(list, data);
        return;
    }
    node_for_insert = (Node*) malloc(sizeof(Node));
    node_for_insert->left = data->left;
    node_for_insert->right = data->right;
    node_for_insert->symbol = data->symbol;
    node_for_insert->frequency = data->frequency;
    node_for_insert->isSymbol = data->isSymbol;

    node_for_insert->p_prev = node_aim_for_insert->p_prev;
    node_aim_for_insert->p_prev->p_next = node_for_insert;

    node_for_insert->p_next = node_aim_for_insert;
    node_aim_for_insert->p_prev = node_for_insert;

    list->size++;

}

void insertion_sort(List **list) {
    List *out = create_list();
    Node *sorted = NULL;
    Node *unsorted = NULL;

    Node* head_out = (*list)->head;
    push_front(out, head_out);
    pop_front(*list);

    unsorted = (*list)->head;
    while (unsorted) {
        sorted = out->head;
        while (sorted && (unsorted->frequency > sorted->frequency)) {
            sorted = sorted->p_next;
        }
        if (sorted) {
            insert_before_element(out, sorted, unsorted);
        } else {
            push_back(out, unsorted);
        }
        unsorted = unsorted->p_next;
    }

    free(*list);
    *list = out;
}

//+------------------------------------------------------+
//|      ========= Double Linked list END ========       |
//+------------------------------------------------------+

#endif // BINARY_TREE_REC_H
