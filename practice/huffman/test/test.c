#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../../test/labtest.h"

#include "fileGenerator.h"
#include "fileHelper.h"


#define HUFFMAN "./huff"

int test1()
{
  //verify extraction and compression
  const char testData[] = "./data";
  const char testOut[] = "./testOut";
  const char testExtracted[] = "./data1";
  randomFile(300, testData);
  compressFile(HUFFMAN, testData, testOut);
  extractFile(HUFFMAN, testOut, testExtracted);
  if(!compareFile(testData, testExtracted) == 0)
    return 1;
  return 0; //return OK
}

int main(int argc, char *argv[])
{
  Test testList[] =
  {
    {test1, 20, "Verify encoded/decoded data"}
  };
  int res = runTests(testList, 1);
  if (res == 0)
  {
    printf("Tests passed successfully!\n");
    return 0;
  }
  printf("Test %d failed: %s\n", res, testList[res - 1].message);
  return res;
}