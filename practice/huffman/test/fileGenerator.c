#include "fileGenerator.h"

void randomFile(uint64_t size, const char * fileName)
{
  uint8_t *data;
  uint64_t i;
  data = malloc(size);
  if(!data)
  {
    printf("error in memory allocation");
    return;
  }
  for(i=0;i<size;i++)
  {
    data[i] = (uint8_t)rand();
  }
  writeFile(data,size,fileName);
  free(data);
  
}

void writeFile(uint8_t *data,uint64_t size, const char * fileName)
{
  FILE *fPtr;
  fPtr = fopen(fileName, "w");
  if(!fPtr)
  {
    printf("error in file creation");
    return;
  }
  fwrite(data, sizeof(uint8_t), size, fPtr);
  fclose(fPtr);
}
