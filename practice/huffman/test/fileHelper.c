#include "fileHelper.h"

int compareFile(const char *inputFile, const char *outputFile)
{
  int i;
  int returnValue =0;
  uint8_t *inputCRC = malloc(sizeof(uint8_t)*MD5_DIGEST_LENGTH);
  uint8_t *outputCRC = malloc(sizeof(uint8_t)*MD5_DIGEST_LENGTH);
  getCRC(inputFile,inputCRC);
  getCRC(outputFile,outputCRC);
  for(i = 0; (i < MD5_DIGEST_LENGTH) && (returnValue == 0); i++)
  {
    if(*(inputCRC+i) != *(outputCRC+i))
    {
      printf("files not match\n");
      returnValue = -1;
    }
  }
  free(inputCRC);
  free(outputCRC);
  return returnValue;
}

void getCRC(const char *fileName,uint8_t *crc)
{
  FILE *inFile = fopen (fileName, "rb");
  MD5_CTX mdContext;
  fseek(inFile, 0L, SEEK_END);
  uint64_t fileSize = ftell(inFile);
  uint8_t *data = malloc(fileSize);
  
  if (inFile == NULL) 
    printf ("%s can't be opened.\n", fileName);

  MD5_Init (&mdContext);
  
  rewind(inFile);
  fread(data, sizeof(uint8_t),fileSize, inFile);
  
  MD5_Update (&mdContext, data, fileSize);
  MD5_Final (crc,&mdContext);
  fclose (inFile);
  free(data);
}

void compressFile(const char *executableFile, const char *inputFile, const char *outputFile)
{
  //./huff ./data2 -c dataOut --dry-run
  char *result = malloc(strlen(executableFile) + strlen(inputFile)
                + strlen(outputFile) + strlen("--dry-run") + strlen("   -c "));
  strcpy(result, executableFile);
  strcat(result, " ");
  strcat(result, inputFile);
  strcat(result, " -c ");
  strcat(result, outputFile);
  strcat(result, " --dry-run");
  system(result);
}

void extractFile(const char *executableFile, const char *inputFile, const char *outputFile)
{
  char *result = malloc(strlen(executableFile) + strlen(inputFile)
                + strlen(outputFile) + strlen("--dry-run") + strlen("   -c "));
  strcpy(result, executableFile);
  strcat(result, " ");
  strcat(result, inputFile);
  strcat(result, " -x ");
  strcat(result, outputFile);
  strcat(result, " --dry-run");
  system(result);
}
