#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <openssl/md5.h>

typedef uint8_t md5crc[MD5_DIGEST_LENGTH];

int compareFile(const char *inputFile, const char *outputFile);
void getCRC(const char *fileName,uint8_t *crc);
void compressFile(const char *executableFile, const char *inputFile, const char *outputFile);
void extractFile(const char *executableFile, const char *inputFile, const char *outputFile);
