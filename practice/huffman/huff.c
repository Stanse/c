#include <stdio.h>
#include <string.h>
#include "binary_tree_rec.h"

#define ERROR_FILE_OPEN -3

char def_path_to_source_file[] = "./data";
char def_path_to_compressed_file[] = "./testOut";
char def_path_to_decompressed_file[] = "./data1";

void print_huffman_tree (Node* root, uint64_t k);
void add_code(Node* head, List* codes_list, uint16_t* i);
void build_table(Node* root, List* tmp_list, List* l_code, uint16_t* i);
void print_huffman_table(List* codes_list, uint16_t size);
void print_bits(List* codes_list, uint16_t size);
void write_sybmol(const char s, List* codes_list, uint8_t characters, FILE* ofile);
void compress_file(List* codes_list, uint16_t size, char* path_to_source, const char* path_to_compressed_f);
void decompress_file(const char* path_to_compressed_f, const char* path_to_decompressed_f, Node* root, uint64_t data_size);


int main(int argc, char** argv)
{
    char* path_to_source_file = NULL;
    char* path_to_compressed_file = NULL;
    char* path_to_extructed_file = NULL;

    int do_compress = 0;
    int do_extruct = 0;

    if(argc == 1)
    {
        do_compress = 1;
        do_extruct = 1;
    }

    if( (argc > 1) && (!strcmp(argv[1], "--help")))
    {
            printf("  <path_to_source_file> <'testOut' - compressed_file>\n");
            printf("  <path_to_compressed_file> <'data1' - extructed_file>\n");
            return 0;
    }
    if(argc > 2)
    {
        if(!strcmp(argv[1], "./data"))
        {
            do_compress = 1;
            do_extruct = 0;
            path_to_source_file = argv[1];
            path_to_compressed_file = def_path_to_compressed_file;
        } else if(!strcmp(argv[1], "./testOut"))
        {
            do_extruct = 1;
            do_compress = 0;
            path_to_compressed_file = argv[1];
            path_to_extructed_file = def_path_to_decompressed_file;
        }
    }


    if(path_to_source_file == NULL)
        path_to_source_file = def_path_to_source_file;
    if(path_to_compressed_file == NULL)
        path_to_compressed_file = def_path_to_compressed_file;
    if(path_to_extructed_file == NULL)
        path_to_extructed_file = def_path_to_decompressed_file;

//    printf("+------------------------------------------------------+\n");
//    printf("|          Reads from file and counts symbols          |\n");
//    printf("+------------------------------------------------------+\n");
    Node* root_of_bin_tree = NULL;

    FILE* source_file;
    source_file = fopen(path_to_source_file, "rb");
    if (source_file == NULL)
    {
        printf("Error opening source file '%s'", path_to_source_file);
        exit(ERROR_FILE_OPEN);
    }

    fseek(source_file, 0L, SEEK_END);
    uint64_t  file_size = ftell(source_file);
    fseek(source_file, 0L, SEEK_SET);

    char symbol;
    uint64_t data_size = 0;
    while(data_size < file_size)
    {
        symbol = (char)fgetc(source_file);
        insert_to_binary_tree(&root_of_bin_tree, symbol);
        data_size++;
    }
    fclose(source_file);

//    in_order_travers(root_of_bin_tree, print_node, NULL);
//    printf("\n  Not sorted binary tree from file:                  \n\n");
//    print_tree(root_of_bin_tree, "'root'", 0);

//    printf("+------------------------------------------------------+\n");
//    printf("|          Create and fill double linked list          |\n");
//    printf("|                  from binary tree                    |\n");
//    printf("+------------------------------------------------------+\n");
    List* list = create_list();
    fill_list(list, root_of_bin_tree);
//    print_list(list->head);

    delete_bin_tree(&root_of_bin_tree);                 // Delete bin tree;

//    printf("+------------------------------------------------------+\n");
//    printf("|           Sorted list by insertion sort              |\n");
//    printf("+------------------------------------------------------+\n");
//    insertion_sort(&list);
//    print_list(list->head);

//    printf("+------------------------------------------------------+\n");
//    printf("|                   Huffman tree                       |\n");
//    printf("+------------------------------------------------------+\n");
    while(list->size != 1)
    {
        insertion_sort(&list);

        Node* left_son = list->head;
        pop_front(list);
        Node* right_son = list->head;
        pop_front(list);

        Node* parent = get_node(0);
        parent->left = left_son;
        parent->right = right_son;
        parent->frequency = right_son->frequency + left_son->frequency;
        parent->isSymbol = 0;
        push_back(list, parent);
    }
    Node* tree_root = list->head;

//    print_tree(list->head, "'root'", 0);
//    printf("________________________________________________________\n");
//    print_huffman_tree(tree_root, 0);

//    printf("+------------------------------------------------------+\n");
//    printf("|                   Huffman table                      |\n");
//    printf("+------------------------------------------------------+\n");

    List* codes_list = NULL;
    codes_list = (List*) malloc(sizeof (List)*512);
    for(uint16_t i = 0; i < 256; i++)
    {
        codes_list[i].head = codes_list[i].tail = 0;
        codes_list[i].size = 0;
    }
    uint16_t characters = 0;
    List* tmp_list = create_list();
    build_table(tree_root, tmp_list, codes_list, &characters);

//    print_huffman_table(codes_list, characters);
//    printf("\n");

//    printf("+------------------------------------------------------+\n");
//    printf("|                    Compress file                     |\n");
//    printf("+------------------------------------------------------+\n");


    if(do_compress)
        compress_file(codes_list, characters, path_to_source_file, path_to_compressed_file);

//    printf("+------------------------------------------------------+\n");
//    printf("|                    Decompress file                   |\n");
//    printf("+------------------------------------------------------+\n");

    if(do_extruct)
        decompress_file(path_to_compressed_file, path_to_extructed_file, tree_root, data_size);

    delete_list(&codes_list);
    return 0;
}

void compress_file(List* codes_list, uint16_t characters, char* path_to_source, const char* path_to_compressed_f)
{
    FILE* compressed_file = NULL;
    compressed_file = fopen(path_to_compressed_f, "wb");
    if (compressed_file == NULL)
    {
        printf("Error opening file: '%s'", path_to_compressed_f);
        exit(ERROR_FILE_OPEN);
    }

    char symbol;
    char byte = 0;
    uint8_t bit_counter = 0;
    uint64_t char_counter = 0;
    uint64_t symbol_counter = 0;
    uint8_t saved_byte = 0;

    FILE* source_file;
    source_file = fopen(path_to_source, "rb");
    if (source_file == NULL)
    {
        printf("Error opening file '%s'", path_to_source);
        exit(ERROR_FILE_OPEN);
    }

    fseek(source_file, 0L, SEEK_END);
    uint64_t  file_size = ftell(source_file);
    fseek(source_file, 0L, SEEK_SET);

    while(char_counter < file_size)
    {
        symbol = (char)fgetc(source_file);
        char_counter++;

        for(uint16_t char_num = 0; char_num < characters; char_num++)
        {
            char tmp_char = codes_list[char_num].head->symbol;
            if(symbol == tmp_char)
            {
                Node* tmp_node = NULL;
                uint16_t code_size = codes_list[char_num].size;

                for(uint16_t j = 0; j < code_size; ++j)
                {
                    tmp_node =  get_by_index(&codes_list[char_num], j);
                    if(tmp_node->frequency == 1)
                    {
                        byte = byte | (char)(1 << (7-bit_counter));
                    }
                    else
                    {
                        byte = byte | (char)(0 << (7-bit_counter));
                    }

                    bit_counter++;
                    if(bit_counter == 8 && (symbol_counter <= file_size))
                    {
                        bit_counter = 0;
                        fwrite(&byte, sizeof (char), 1, compressed_file);
                        byte = 0;
                        saved_byte++;
                    }
                }
                symbol_counter++;
                if(bit_counter == 8 && (symbol_counter <= file_size))
                {
                    bit_counter = 0;
                    fwrite(&byte, sizeof (char), 1, compressed_file);
                    byte = 0;
                    saved_byte++;
                }
                break;
            }

        }
    }

    if(bit_counter > 0 && (symbol_counter <= file_size))
    {
        fwrite(&byte, sizeof (char), 1, compressed_file);
        saved_byte++;
    }
    fclose(source_file);
    if(!fclose(compressed_file))
         printf("Compressed file: '%s' was created. \n", path_to_compressed_f);
    else
        printf("ERROR: compressed file isn't created.");

}

void decompress_file(const char* path_to_compressed_f, const char* path_to_decompressed_f, Node* root, uint64_t data_size)
{
    FILE* compressed_file = NULL;
    compressed_file = fopen(path_to_compressed_f, "rb");
    if (compressed_file == NULL)
    {
        printf("Error opening file '%s'", path_to_compressed_f);
        exit(ERROR_FILE_OPEN);
    }

    FILE* extructed_file = NULL;
    extructed_file = fopen(path_to_decompressed_f, "wb");
    if (extructed_file == NULL)
    {
        printf("Error opening file '%s'", path_to_decompressed_f);
        exit(ERROR_FILE_OPEN);
    }

    char byte = 0;
    uint8_t bit_counter = 0;
    uint64_t char_counter = 0;

    Node* node = root;
    while(char_counter < data_size)
    {
        byte = (char)fgetc(compressed_file);
        bit_counter = 0;
        char current_symbol = 0;
        while(bit_counter < 8)
        {
            char is_right = byte & (1 << (7-bit_counter));
            if(is_right)
                node = node->right;
            else
                node = node->left;
            if(node->isSymbol)
            {
                current_symbol = node->symbol;
                //printf("%c", current_symbol);
                fwrite(&current_symbol, sizeof (char), 1, extructed_file);
                node = root;
                char_counter++;
            }
            if(char_counter == data_size)
                break;
            bit_counter++;
        }
    }

    fclose(compressed_file);
    if(!fclose(extructed_file))
         printf("Decompressed file: '%s' was extracted.\n" , path_to_decompressed_f);
    else
        printf("ERROR: File isn't extracted to 'data1'.");
}

void print_huffman_tree (Node* root, uint64_t k)
{
    if(root != NULL)
    {
        print_huffman_tree(root->left, k + 3);

    for(uint64_t i = 0; i < k; i++)
    {
        printf("  ");
    }
    if(root->isSymbol)
        printf("%lu('%c' = '0x%x') \n", root->frequency, root->symbol, root->symbol);
    else
        printf("%lu \n" , root->frequency);
    print_huffman_tree(root->right, k + 3);
    }
}

void add_code(Node* head, List* codes_list, uint16_t* i)
{
    while(head)
    {
        push_back(&codes_list[*i], head);
        head = head->p_next;
    }
}

void build_table(Node* root, List* tmp_list, List* l_code, uint16_t* i)
{
    if(root->left != NULL)
    {
        Node* tmp = get_node(0);
        tmp->frequency = 0;
        push_back(tmp_list, tmp);
        build_table(root->left, tmp_list, l_code, i);
    }
    if(root->right != NULL)
    {
        Node* tmp = get_node(0);
        tmp->frequency = 1;
        push_back(tmp_list, tmp);
        build_table(root->right, tmp_list, l_code, i);
    }
    if(root->isSymbol)
    {
        tmp_list->head->symbol = root->symbol;
        add_code(tmp_list->head, l_code, i);
        *i += 1;
    }
    if(tmp_list->size > 0)
    {
        pop_back(tmp_list);
    }
}

void print_huffman_table(List* codes_list, uint16_t size)
{
    for(uint16_t i = 0; i < size; ++i)
    {
//        if(codes_list[i].head->symbol == '\n')
//            printf("'\\n' = '0x%X'    = ", codes_list[i].head->symbol);
//        else
            printf("'%c' = '0x%X'    = ", codes_list[i].head->symbol, codes_list[i].head->symbol);

        Node* tmp = NULL;
        uint16_t code_size = codes_list[i].size;
        for(uint16_t j = 0; j < code_size; ++j)
        {
            tmp =  get_by_index(&codes_list[i], j);
            printf("%lu", tmp->frequency);
        }
        printf("\n");
    }
}

void print_bits(List* codes_list, uint16_t size)
{
    for(uint16_t i = 0; i < size; ++i)
    {
        Node* tmp = NULL;
        uint16_t code_size = codes_list[i].size;
        for(uint16_t j = 0; j < code_size; ++j)
        {
            tmp =  get_by_index(&codes_list[i], j);
            printf("%lu", tmp->frequency);
        }
    }
    printf("\n\n");
}


