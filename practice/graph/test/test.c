#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "answer.h"

int test1(void) {
    // reserve memory with more than 1500 extra symbols for a case if
    // student will want to play with its header in output
    char appOut[18000] = {0};
    int i;
    const unsigned char key = 0x2a;
    int outSize;

    FILE* handler = popen("./../graph", "r");
    if (handler == NULL) return 1;

    outSize = fread(appOut, sizeof(char), sizeof(appOut), handler);
    pclose(handler);

    // -1 is an index to null terminator. -2 is a new line symbol.
    size_t lastCorrectAnswerIndex = sizeof(answer) - 2;

    // -1 is an index to a new line symbol. We didn't read null terminator from a stream so we don't
    // have it here unlike answer, that is null terminated string and has this null terminator.
    size_t lastStudentAnswerIndex = outSize - 1;
    char* lastStudentAnswerElement = appOut + lastStudentAnswerIndex;
    char* lastCorrectAnswerElement = answer + lastCorrectAnswerIndex;
    size_t countElementsToCheck = 2000;

    if (outSize < 15000) return 2;  // in correct answer more than 15000 symbols
    if (strncmp(lastStudentAnswerElement - countElementsToCheck,
                lastCorrectAnswerElement - countElementsToCheck, countElementsToCheck) == 0) {
        return 0;
    } else {
        return 3;
    }    
}

int main(int argc, char* argv[]) {
    return test1();
}
