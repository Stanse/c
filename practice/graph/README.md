Implement a simple application which calculates the following function for numbers in range [0 ... 2000) (known as a133058 at OEIS):

a(0)=a(1)=1; for n>1, a(n) = a(n-1) + n + 1 if a(n-1) and n are coprime, otherwise a(n) = a(n-1)/GCD(a(n-1),n)

GCD means greatest common divisor. Coprime numbers are those which have GCD equal to 1.
Sceleton code is provided. The easiest way to plot it is using gnuplot (example is in Makefile) but you can you anything else.
Can you explain what happens with the plot at x equal 638?
