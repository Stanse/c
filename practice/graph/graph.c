#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>

#define N 2000
static uint32_t w = 500;
static uint32_t h = 500;
static const uint32_t size = 2500000;

struct color{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};
static struct color color_background;
static struct color color_graph;


static void plot(int *a, size_t len){
    fprintf(stdout,
            "set datafile separator ',' \n"
            //"set terminal png size 1024, 768\n"
            "set title '%s'\n"
            "set xlabel '%s'\n"
            "set ylabel '%s'\n"
            "set key left top\n"
            "set grid\n",
            "Fly straight, damn it!", "x", "y"
            );
    fprintf(stdout, "plot '-' with points title 'samples' pt 7\n");
    for(size_t i = 0; i < len; i++){
        fprintf(stdout, "%zu,%d\n", i, a[i]);
    }
}

int save_graph_to_file(struct color *buffer[]){
    FILE *file_graph = fopen("graph.ppm", "wb");
    char ch1[16] = "P6\n"; //500 500 255\n
    char str[12];
    sprintf(str, "%d", w);
    strcat(ch1, str);
    strcat(ch1, " ");
    sprintf(str, "%d", h);
    strcat(ch1, str);
    strcat(ch1, " 255\n");

   // printf("%s", ch1);
    if(file_graph == NULL){
      perror("error: fopen() ");
      return 1;
    } else {
        fprintf(file_graph, "%s", ch1);
        fwrite(buffer, sizeof(color_background), size, file_graph);
    }
    fclose(file_graph);
    return 0;
}

int find_gcd(int a, int b){
    while(1){
        while (a && b)
                if (a >= b)
                   a %= b;
                else
                   b %= a;
            return a | b;
    }
}

int main()
{
    color_background.r = 50;
    color_background.g = 80;
    color_background.b = 80;

    color_graph.r = 255;
    color_graph.g = 0;
    color_graph.b = 0;


    struct color buffer[size];

    for(uint32_t i = 0; i < size; i++){
        buffer[i] = color_background;
    }


    int arr[N] = {0};
    arr[0] = 1;
    arr[1] = 1;

    int GCD = 0;

    for (int i = 2; i < N; i++) {
        GCD = find_gcd(arr[i-1], i);
        if(GCD != 1){
            arr[i] = arr[i-1]/GCD;
        } else {           
            arr[i] = arr[i-1] + i + 1;
        }
    }

    for(uint32_t i = 0; i < N; i++){
        // отнимаем от h - 10, чтобы горизонтальняю линию было лучше видно
        uint32_t indx = ((h - 10 - arr[i]/10)*(w) + (i)/4);
        buffer[indx] = color_graph;
    }

    save_graph_to_file(buffer);

    plot(arr, N);

    return 0;
}
