#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "../../../test/labtest.h"


bool areBoardsEqual(int columns, int rows, char lhs[columns][rows], char rhs[columns][rows]){
	for(int col = 0; col < columns; ++col){
		for(int row = 0; row < rows; ++row){
			if(lhs[col][row] != rhs[col][row]){

				return false;			
			}
		}
	}

	return true;
}

int testGlider(void)
{
	// Draw glider
	char board[4][4] = {0, 1, 0, 0,
						0, 0, 1, 0,
						1, 1, 1, 0,
						0, 0, 0, 0};

	char step1[4][4] = {0, 0, 0, 0,
						1, 0, 1, 0,
						0, 1, 1, 0,
						0, 1, 0, 0};

	step(4, 4, board);
	if (!areBoardsEqual(4, 4, board, step1)){
		return 1;
	}

	char step2[4][4] = {0, 0, 0, 0,
						0, 0, 1, 0,
						1, 0, 1, 0,
						0, 1, 1, 0};

	step(4, 4, board);
	if (!areBoardsEqual(4, 4, board, step2)){
		return 2;
	}


	char step3[4][4] = {0, 0, 0, 0,
						0, 1, 0, 0,
						0, 0, 1, 1,
						0, 1, 1, 0};

	step(4, 4, board);
	if (!areBoardsEqual(4, 4, board, step3)){
		return 3;
	}


	char step4[4][4] = {0, 0, 0, 0,
						0, 0, 1, 0,
						0, 0, 0, 1,
						0, 1, 1, 1};

	step(4, 4, board);
	if (!areBoardsEqual(4, 4, board, step4)){
		return 4;
	}


	return 0;
}

int main(int argc, char *argv[])
{
  Test testList[] =
  {
    {testGlider, 20, "Glider does not work"}
  };
  int res = runTests(testList, 1);
  if (res == 0)
  {
    printf("Tests passed successfully!\n");
    return 0;
  }
  printf("Test %d failed: %s\n", res, testList[res - 1].message);
  return res;
}
