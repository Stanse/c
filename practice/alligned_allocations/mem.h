#include <stdlib.h>

void *malloc_alligned(size_t nmemb, size_t alignment);
void free_alligned(void *p);
