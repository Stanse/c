#include <stdio.h>
#include "mem.h"

int main (int argc, char *argv[])
{
    char **endptr;
    void *p = malloc_alligned(42, 1024);

    printf("%p\n", p);

    if (!p) {
        return -1;
    }

    free_alligned(p);

    return 0;
}
